// db.js
import mysql from 'mysql2';
import { promisify } from 'util';
import { dbConfig } from './settings';

export const pool = mysql.createPool({
  // host: 'localhost',
  // user: 'root',
  // password: 'your_password',
  // database: 'your_database',
  ...dbConfig,
  waitForConnections: true,
  connectionLimit: 10,
  queueLimit: 0,
});

export const queryAsync = promisify(pool.query).bind(pool);
