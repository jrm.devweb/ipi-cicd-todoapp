import PropTypes from 'prop-types';

import './Footer.css';

/**
 * Le composant Footer reçoit :
 * - SOIT une erreur (error) : un objet avec une propriété message
 * - SOIT un message (message) : une chaîne de caractères
 * Ça ne peut pas être les deux à la fois.
 */
export default function Footer({ error, message }) {
  const className = error ? 'text-error' : 'text-success';
  return (
    <footer className="Footer">
      <p className={className}>
        {/* SI on reçoit une erreur, on l'affiche  */}
        {error && (
          <>
            <span>Error fetching message:</span>{' '}
            <span id="footerError" className="text-bold">{error.message}</span>
          </>
        )}
        {message && (
          <>
            <span>Message from server:</span>{' '}
            <span id="serverMessage" className="text-bold">
              {message}
            </span>
          </>
        )}
      </p>
    </footer>
  );
}

Footer.propTypes = {
  error: PropTypes.shape({
    message: PropTypes.string.isRequired,
  }),
  message: PropTypes.string,
};
