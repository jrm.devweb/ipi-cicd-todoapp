import PropTypes from 'prop-types';

import "./AddTodo.css";

export default function AddTodo({ name, setName, handleAddItem }) {
  return (
    <form onSubmit={handleAddItem}>
      <input
        id="itemName"
        className="AddTodo-itemName"
        type="text"
        value={name}
        onChange={(e) => setName(e.target.value)}
      />
      <button id="submitItem" type="submit">
        Add Task
      </button>
    </form>
  );
}

AddTodo.propTypes = {
  name: PropTypes.string.isRequired,
  setName: PropTypes.func.isRequired,
  handleAddItem: PropTypes.func.isRequired,
};
